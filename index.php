<!DOCTYPE html>
<html lang="en">
    
<head>
    <title>Product List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
    <link href="/addproduct/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    
    <!-- Top -->
    <div class="row">
        <div class="col-md-10">
            <h1>Product List</h1>
        </div>
        <div class="col-md-2 buttons">
            <a href="/addproduct"><button>ADD</button></a>
            <button type="submit" name="delete" form="del">MASS DELETE</button>
        </div>
    </div>
    
    <hr class="solid">
    
    <!-- Items-->
    <form method="post" id="del">
        <div class="items">
            <div class="container">
                <div class="row">
                    <?php
                        // Deleting selected items
	    				include('db.php');
		    		    if(isset($_POST['delete'])){
			    			$checkbox = $_POST['check'];
				    		for($i=0;$i<count($checkbox);$i++){
					    		$del_id = $checkbox[$i]; 
						    	mysqli_query($conn,"DELETE FROM products WHERE SKU='".$del_id."'");
    						}
	    				}
	    				
	    				// Selecting items for display
		    			$sql = "SELECT products.*, book.Weight AS book , dvd.Size AS dvd , furniture.Dimensions AS furniture FROM products LEFT JOIN dvd ON products.sku = dvd.sku LEFT JOIN book ON products.sku = book.sku LEFT JOIN furniture ON products.sku = furniture.sku";
    					$result = $conn->query($sql);

	    				if ($result->num_rows > 0) {
		    				$i=0;
			    			while($row = mysqli_fetch_array($result)) :
			    			    if ($row['Type'] == "book") {
			    			        $a = "Weight:&nbsp;";
			    			        $b = "KG";
			    			    }
			    			    elseif ($row['Type'] == "dvd") {
			    			        $a = "Size:&nbsp;";
			    			        $b = "&nbsp;MB";
			    			    }
			    			    elseif ($row['Type'] == "furniture") {
			    			        $a = "Dimensins:&nbsp;";
			    			        $b = "";
			    			    }
				    			?>
					    		<div class="col-md-4">
						    		<div class="grid-container">
										<div class="card">
											<input class="delete-checkbox" type="checkbox" id="checkItem" name="check[]" value="<?php echo $row['SKU']; ?>">                       
											<div class="card-body"><?php echo $row['SKU'];?></div>
											<div class="card-body"><?php echo $row['Name'];?></div>
											<div class="card-body"><p><?php echo $row['Price'];?> $</p></div>
											<div class="card-body"><p><?php echo $a, $row[$row['Type']], $b; $i++;?></p></div>
										</div>
                                    </div>
                                </div>
                                <?php
						    endwhile;
					    }
				    ?>
				</div>
            </div>
        </div>
    </form>
    
    <!-- Footer -->
    <div id="footer">
        <hr class="solid">
        <div class="container text-center">
            <div class="col">
                <p>Scandiweb Test assignment</p>
            </div>
        </div>
    </div>
    
</body>