<?php
ob_start();
?>
<!DOCTYPE HTML5>
<html lang="en">
    
<head>
    <title>Add Product</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
    <!-- Top -->
    <div class="row">
        <div class="col-md-10">
            <h1>Product Add</h1>
        </div>
        <div class="col-md-2 buttons">
            <button type="submit" name="submit" form="product_form" onclick="submit()">Save</button>
            <a href="/.."><button>Cancel</button></a>
        </div>
    </div>
    
    <hr class="solid">
    
    <!-- Items -->
    <form id="product_form" method="post">
        <label for="sku">SKU </label>
        <input type="text" id="sku" name="sku" value="<?php if (!empty($_POST["sku"])) { echo $_POST["sku"]; } else { echo ''; };  ?>" required>
        <br><br>
        <label for="name">Name </label>
        <input type="text" id="name" name="name" value="<?php if (!empty($_POST["name"])) { echo $_POST["name"]; } else { echo ''; };  ?>" required>
        <br><br>
        <label for="price">Price </label>
        <input type="number" step="0.01" id="price" name="price" value="<?php if (!empty($_POST["price"])) { echo $_POST["price"]; } else { echo ''; };  ?>" required>
        <br><br>
        <label for="productType">Type </label>
        <select name="productType" id="productType" value="book" required>
            <?php $selected = $_POST['productType'];?>
            <option value="">-</option>
            <option value="dvd" <?php if($selected == 'dvd'){echo("selected");}?>>DVD</option>
            <option value="book" <?php if($selected == 'book'){echo("selected");}?>>Book</option>
            <option value="furniture" <?php if($selected == 'furniture'){echo("selected");}?>>Furniture</option>
        </select>
        <br><br>
        <div id ="nextStep"></div>
    </form>
    
    <!-- Script for dynamic form -->
    <script>
        $('#productType').click(function(){
            if($(this).val() == 'dvd'){
                $('#nextStep').html('<label for="size">Size (MB) </label><input type="number" step="0.001" id="size" name="size" value="<?php if (!empty($_POST["size"])) { echo $_POST["size"]; } else { echo ''; };  ?>" required><br><br><p class="note">*Please, provide size*</p>');
            }
            if($(this).val() == 'book'){
                $('#nextStep').html('<label for="weight">Weight (KG) </label><input type="number" step="0.001" id="weight" name="weight" value="<?php if (!empty($_POST["weight"])) { echo $_POST["weight"]; } else { echo ''; };  ?>" required><br><br><p class="note">*Please, provide weight*</p>');
            }
            if($(this).val() == 'furniture'){
                $('#nextStep').html('<label for="height">Height (CM) </label><input type="number" step="0.001" id="height" name="height" value="<?php if (!empty($_POST["height"])) { echo $_POST["height"]; } else { echo ''; };  ?>" required><br><br><label for="width">Width (CM) </label><input type="number" step="0.001" id="width" name="width" value="<?php if (!empty($_POST["width"])) { echo $_POST["width"]; } else { echo ''; };  ?>" required><br><br><label for="length">Length (CM) </label><input type="number" step="0.001" id="length" name="length" value="<?php if (!empty($_POST["length"])) { echo $_POST["length"]; } else { echo ''; };  ?>" required><br><br><p class="note">*Please, provide dimensions*</p>');
            }
        });
    </script>
    
    <!-- Data insert -->
    <?php
    include "db.php";

    if(isset($_POST['submit']))
    {		
        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $productType = $_POST['productType'];
        $size = $_POST['size'];
        $weight = $_POST['weight'];
        $height = $_POST['height'];
        $width = $_POST['width'];
        $length = $_POST['length'];
        $dimensions = $height . "x" . $width . "x" . $length;
    
        if(isset($sku) && isset($name) && isset($price) && isset($productType)) {$products = "INSERT INTO products (SKU,Name,Price,Type) VALUES ( '". $sku ."', '". $name ."', '". $price ."', '". $productType ."')";}
        
        if ($conn->query($products) === TRUE) {
            if(isset($size)) {$dvd = "INSERT INTO dvd (SKU,Size) VALUES ( '". $sku ."', '". $size ."')";}
            if(isset($weight)) {$book = "INSERT INTO book (SKU,Weight) VALUES ( '". $sku ."', '". $weight ."')";}
            if(isset($height) && isset($width) && isset($length)) {$furniture = "INSERT INTO furniture (SKU,Dimensions) VALUES ( '". $sku ."', '". $dimensions ."')";}
            
            if ($conn->query($dvd) === TRUE) {
                mysqli_close($db);
                header('location: /..');
            }
            if ($conn->query($book) === TRUE) {
                
                mysqli_close($db);
                header('location: /..');
            }
            if ($conn->query($furniture) === TRUE) {
                mysqli_close($db);
                header('location: /..');
            }
        } else {
            echo "Error: " . $conn->error;
        }
    }
    ?>

    <!-- Footer -->
    <div id="footer">
        <hr class="solid">
        <div class="container text-center">
            <div class="col">
                <p>Scandiweb Test assignment</p>
            </div>
        </div>
    </div>
    
</body>